#!/usr/bin/env python
# coding: utf-8

# In[7]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
from tqdm import tqdm_notebook

from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import BernoulliNB
import eli5
from scipy.sparse import hstack, vstack

from wordcloud import WordCloud, STOPWORDS
import plotly.graph_objs as go
from plotly import tools
import plotly.offline as py
py.init_notebook_mode(connected=True)


# In[35]:


PATH = ("C:\Users\baral\Downloads\CSV\")


# In[17]:


train_text = []
test_text = []
train_label = []
test_label = []

for train_test in ['train','test']:
    for neg_pos in ['neg','pos']:
        file_path = PATH + train_test + '/' + neg_pos + '/'
        for file in tqdm_notebook(os.listdir(file_path)):
            with open(file_path + file, 'r') as f:
                if train_test == 'train':
                    train_text.append(f.read())
                    if neg_pos == 'neg':
                        train_label.append(0)
                    else:
                        train_label.append(1)
                else:
                    test_text.append(f.read())
                    if neg_pos == 'neg':
                        test_label.append(0)
                    else:
                        test_label.append(1)


# In[18]:


X_train = pd.DataFrame()
X_train['review'] = train_text
X_train['label'] = train_label

X_test = pd.DataFrame()
X_test['review'] = test_text
X_test['label'] = test_label


# In[19]:


X_train.head()


# In[20]:


sns.countplot(X_train['label']);


# In[ ]:


X_train[X_train['label']==0]['review'].apply(lambda x: np.log1p(len(x))).hist(alpha=0.6, color='red', label='Negative')
X_train[X_train['label']==1]['review'].apply(lambda x: np.log1p(len(x))).hist(alpha=0.6, color='green', label='Positive')
plt.title('Character count')
plt.legend();


# In[21]:


X_train[X_train['label']==0]['review'].apply(lambda x: np.log1p(len(x.split()))).hist(alpha=0.6, color='red', label='Negative')
X_train[X_train['label']==1]['review'].apply(lambda x: np.log1p(len(x.split()))).hist(alpha=0.6, color='green', label='Positive')
plt.title('Word count')
plt.legend();


# In[22]:


X_train[X_train['label']==0]['review'].apply(lambda x: np.log1p(len([word for word in x.split() if word.istitle()]))).hist(alpha=0.6, color='red', label='Negative')
X_train[X_train['label']==1]['review'].apply(lambda x: np.log1p(len([word for word in x.split() if word.istitle()]))).hist(alpha=0.6, color='green', label='Positive')
plt.title('Title word count')
plt.legend();


# In[23]:


X_train[X_train['label']==0]['review'].apply(lambda x: np.log1p(len([word for word in x.split() if word.isupper()]))).hist(alpha=0.6, color='red', label='Negative')
X_train[X_train['label']==1]['review'].apply(lambda x: np.log1p(len([word for word in x.split() if word.isupper()]))).hist(alpha=0.6, color='green', label='Positive')
plt.title('Upper Case Word count')
plt.legend();


# In[24]:


X_train[X_train['label']==0]['review'].apply(lambda x: np.log1p(len(''.join(_ for _ in x if _ in string.punctuation)))).hist(alpha=0.6, color='red', label='Negative')
X_train[X_train['label']==1]['review'].apply(lambda x: np.log1p(len(''.join(_ for _ in x if _ in string.punctuation)))).hist(alpha=0.6, color='green', label='Positive')
plt.title('Punctuation Character count')
plt.legend();


# In[25]:


def plot_wordcloud(data, title):
    wordcloud = WordCloud(background_color='black',
                          stopwords=set(STOPWORDS),
                          max_words=200,
                          max_font_size=100,
                          random_state=17,
                          width=800,
                          height=400,
                          mask=None)
    wordcloud.generate(str(data))
    plt.figure(figsize=(15.0,10.0))
    plt.axis('off')
    plt.title(title)
    plt.imshow(wordcloud);


# In[26]:


plot_wordcloud(X_train[X_train['label']==0]['review'], 'Negative IMDB reviews')


# In[27]:


plot_wordcloud(X_train[X_train['label']==1]['review'], 'Positive IMDB reviews')


# In[28]:


X_train_neg = X_train[X_train['label']==0]
X_train_pos = X_train[X_train['label']==1]
additional_stopwords = ['<br', '-', '/><br', '/>the', '/>this']

def generate_ngrams(text, n_gram=1):
    token = [token for token in text.lower().split(' ') if token != '' if token not in STOPWORDS if token not in additional_stopwords]
    ngrams = zip(*[token[i:] for i in range(n_gram)])
    return [' '.join(ngram) for ngram in ngrams]

def horizontal_bar_chart(df, color):
    trace = go.Bar(
        y=df['word'].values[::-1],
        x=df['wordcount'].values[::-1],
        showlegend=False,
        orientation = 'h',
        marker=dict(
            color=color,
        ),
    )
    return trace


# In[29]:


freq_dict = defaultdict(int)
for sent in X_train_neg['review']:
    for word in generate_ngrams(sent,1):
        freq_dict[word] += 1
fd_sorted = pd.DataFrame(sorted(freq_dict.items(), key=lambda x: x[1])[::-1])
fd_sorted.columns = ['word', 'wordcount']
trace0 = horizontal_bar_chart(fd_sorted.head(20), 'red')

freq_dict = defaultdict(int)
for sent in X_train_pos['review']:
    for word in generate_ngrams(sent,1):
        freq_dict[word] += 1
fd_sorted = pd.DataFrame(sorted(freq_dict.items(), key=lambda x: x[1])[::-1])
fd_sorted.columns = ['word', 'wordcount']
trace1 = horizontal_bar_chart(fd_sorted.head(20), 'green')

# Creating two subplots
fig = tools.make_subplots(rows=1, cols=2, vertical_spacing=0.04,horizontal_spacing=0.15,
                          subplot_titles=['Frequent unigrams within negative reviews', 
                                          'Frequent unigrams within positive reviews'])
fig.append_trace(trace0, 1, 1)
fig.append_trace(trace1, 1, 2)
fig['layout'].update(height=600, width=900, paper_bgcolor='rgb(233,233,233)', title='Unigram Count Plots')
py.iplot(fig, filename='word-plots')


# In[30]:


freq_dict = defaultdict(int)
for sent in X_train_neg['review']:
    for word in generate_ngrams(sent,2):
        freq_dict[word] += 1
fd_sorted = pd.DataFrame(sorted(freq_dict.items(), key=lambda x: x[1])[::-1])
fd_sorted.columns = ['word', 'wordcount']
trace0 = horizontal_bar_chart(fd_sorted.head(20), 'red')

freq_dict = defaultdict(int)
for sent in X_train_pos['review']:
    for word in generate_ngrams(sent,2):
        freq_dict[word] += 1
fd_sorted = pd.DataFrame(sorted(freq_dict.items(), key=lambda x: x[1])[::-1])
fd_sorted.columns = ['word', 'wordcount']
trace1 = horizontal_bar_chart(fd_sorted.head(20), 'green')

# Creating two subplots
fig = tools.make_subplots(rows=1, cols=2, vertical_spacing=0.04,horizontal_spacing=0.15,
                          subplot_titles=['Frequent bigrams within negative reviews', 
                                          'Frequent bigrams within positive reviews'])
fig.append_trace(trace0, 1, 1)
fig.append_trace(trace1, 1, 2)
fig['layout'].update(height=600, width=900, paper_bgcolor='rgb(233,233,233)', title='Bigram Count Plots')
py.iplot(fig, filename='word-plots')


# In[ ]:




